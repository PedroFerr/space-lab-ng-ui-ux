import { Component, OnInit } from '@angular/core';
import { Bonus } from './app.interfaces';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    title = 'rad2-challenge';
    // The Array with all possible (collection) bonus JSONs
    bonusColl = null as Bonus[];
    // Once random chosen, this var will memorize it - so we don't repaet it on next call:
    randomIdx: number;
    // And, finally, our random Bonus JSON to render on <bonus-default /> and <bonus-ticked /> components:
    currentBonus: Bonus = null;

    // Has user clicked on "Claim now" button? If so, next is "true"
    isBonusTicked = false;

    constructor() { }

    ngOnInit() {
        this.getBonusColl();
    }

    getBonusColl() {

        const ajaxRequest = new XMLHttpRequest();

        ajaxRequest.onreadystatechange = () => {

            if (ajaxRequest.readyState === 4) {

                if (ajaxRequest.status === 200) {
                    const jsonObj = JSON.parse(ajaxRequest.responseText);
                    this.bonusColl = jsonObj.bonusTypes;
                    // Pick one:
                    this.getRandomBonus();

                } else {
                    console.log('Status error: ' + ajaxRequest.status);
                }

            } else {
                console.log('Ignored readyState: ' + ajaxRequest.readyState);
            }


        };

        ajaxRequest.open('GET', 'assets/_data/bonus-to-claim.json');
        ajaxRequest.send();
    }

    getRandomBonus() {
        const rIdx = Math.floor(Math.random() * this.bonusColl.length);
        // Make sure it's different from the previous:
        this.randomIdx = this.randomIdx !== rIdx ?
            rIdx
            :
            this.randomIdx + 1 > this.bonusColl.length - 1 ?
                0
                :
                this.randomIdx + 1
        ;
        this.currentBonus = this.bonusColl[this.randomIdx];
    }

    onBonusDefaultClick() {
        this.isBonusTicked = true;
    }

}
