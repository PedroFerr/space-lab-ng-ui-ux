import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { BonusDefaultComponent } from './ui-components/bonus-default/bonus-default.component';
import { BonusTickedComponent } from './ui-components/bonus-ticked/bonus-ticked.component';
import { UiButtonComponent } from './ui-components/ui-button/ui-button.component';

@NgModule({
    declarations: [
        AppComponent
        , BonusDefaultComponent
        , BonusTickedComponent
        , UiButtonComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
