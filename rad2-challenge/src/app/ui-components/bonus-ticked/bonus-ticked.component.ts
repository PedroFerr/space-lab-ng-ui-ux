import { Component, OnChanges, Input } from '@angular/core';
import { Bonus } from 'src/app/app.interfaces';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'bonus-ticked',
    templateUrl: './bonus-ticked.component.html',
    styleUrls: ['./bonus-ticked.component.scss']
})
export class BonusTickedComponent implements OnChanges {

    @Input() randomBonus: Bonus;

    prizePrefix: string;
    prizeSufix: string;

    constructor() { }

    ngOnChanges() {
        if (this.randomBonus) {
            const aTemp = this.randomBonus.prize.split(' ');
            this.prizePrefix = aTemp[0];
            this.prizeSufix = aTemp[1];
        }
    }

}
