import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonusTickedComponent } from './bonus-ticked.component';

describe('BonusTickedComponent', () => {
  let component: BonusTickedComponent;
  let fixture: ComponentFixture<BonusTickedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonusTickedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonusTickedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
