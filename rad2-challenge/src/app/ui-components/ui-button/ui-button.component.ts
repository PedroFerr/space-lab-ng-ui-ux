import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'ui-button',
    templateUrl: './ui-button.component.html',
    styleUrls: ['./ui-button.component.scss']
})
export class UiButtonComponent {

    @Input() cssClass: 'default' | 'ticked' | null;
    @Input() buttonText: string | null;

    @Output() buttonClick = new EventEmitter<boolean>();

    constructor() { }

    onClickEmitBack() {
        this.buttonClick.emit(true);
    }
}
