import { Component, Optional, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Bonus } from 'src/app/app.interfaces';
// We'll need some Methods from main App component - to refresh random Bonus:
import { AppComponent } from '../../app.component';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'bonus-default',
    templateUrl: './bonus-default.component.html',
    styleUrls: ['./bonus-default.component.scss']
})
export class BonusDefaultComponent implements OnChanges {

    @Input() randomBonus: Bonus;
    @Output() buttonClick = new EventEmitter<boolean>();

    constructor(
        // @Host() private appComponent: AppComponent <= throws/can throw an ERROR.
        /*
        * If "AppComponent" can not (still...) be found by Angular, an error will be thrown, on mounting it!
        * And it can happen NO ERROR is thrown at all - simply THIS Component won't work at all, which is even more scary...
         * "Yes you can use (and MUST, here)
         *      constructor (@Host() @SkipSelf() @Optional() toys: ToysService) {...}"
         * " - check it here: https://medium.com/frontend-coach/self-or-optional-host-the-visual-guide-to-angular-di-decorators-73fbbb5c8658
        */
        @Optional() private appComponent: AppComponent
    ) {}

    ngOnChanges() {
        // User might have clicked, before reaching "00:00", and component is not rendered anymore:
        if (this.randomBonus) {
            const cDownSpan = document.getElementById('countdown');
            // Start the countdown:
            let timeleft = 25;
            const downloadTimer = setInterval(() => {
                timeleft--;
                // Having always 2 digits
                // REMEMBER user might have clicked, during count down, and component is not rendered anymore:
                if (cDownSpan) {
                    cDownSpan.textContent = timeleft < 10 ? `0${timeleft.toString()}` : timeleft.toString();
                }
                if (timeleft < 0) {
                    clearInterval(downloadTimer);
                    cDownSpan.textContent = '25';
                    // Get another Bonus panel - refresh parent component data:
                    this.appComponent.getRandomBonus();
                    // We could, instead, emit back a boolean to App component, and, once received, call this Method from there...
                }

            }, 1000);
        }
    }

    onButtonClick(evt: boolean) {
        this.buttonClick.emit(true);
    }

}
